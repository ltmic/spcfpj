package com.penghaisoft.task.service.impl;

import com.penghaisoft.task.dao.SPCEquipmentSparkleDataMapper;
import com.penghaisoft.task.entity.SPCEquipmentSparkleData;
import com.penghaisoft.task.service.ISPCEquipmentSparkleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description 既存数据接口
 * @ClassName ISPCEquipmentSparkleServiceImpl
 * @Author luot
 * @Date 2020/8/18 17:55
 **/
@Service
public class SPCEquipmentSparkleServiceImpl implements ISPCEquipmentSparkleService {

    @Autowired
    private SPCEquipmentSparkleDataMapper spcEquipmentSparkleDataMapper;

    /**
     * @Description 获取最后一条数据
     * @Author luot
     * @Date 2020/8/18 17:56
     * @Param
     * @return
     **/
    @Override
    public SPCEquipmentSparkleData selectTop1(String equipmentno){
        return spcEquipmentSparkleDataMapper.selectTop1(equipmentno);
    }

    /**
      * @Description 插入数据
      * @Author luot
      * @Date 2020/8/20 17:58
      * @Param
      * @return
      **/
    @Override
    public int insertSelective(SPCEquipmentSparkleData record){
        return spcEquipmentSparkleDataMapper.insertSelective(record);
    }
}
