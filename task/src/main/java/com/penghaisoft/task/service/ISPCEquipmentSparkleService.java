package com.penghaisoft.task.service;

import com.penghaisoft.task.entity.SPCEquipmentSparkleData;

/**
  * @Description 既存数据接口
  * @ClassName ISPCEquipmentSparkleServiceImpl
  * @Author luot
  * @Date 2020/8/18 17:55
  **/
public interface ISPCEquipmentSparkleService {
    /**
      * @Description 获取最后一条数据
      * @Author luot
      * @Date 2020/8/18 17:56
      * @Param
      * @return
      **/
    SPCEquipmentSparkleData selectTop1(String equipmentno);

    /**
      * @Description 插入数据
      * @Author luot
      * @Date 2020/8/20 17:59
      * @Param
      * @return
      **/
    int insertSelective(SPCEquipmentSparkleData record);
}
