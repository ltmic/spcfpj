package com.penghaisoft.task.feign.client;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Value;

/**
 * @ClassName Feign配置 设置请求token
 * @Description
 * @Author luo_0
 * @Date 2020/8/17 16:11
 **/
public class FeignConfig implements RequestInterceptor {

    @Value("${feign.hollysys.token}")
    private String token;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("token", token);
    }
}
