package com.penghaisoft.task.feign.client;

import com.penghaisoft.task.entity.RtnObjHollysys;
import com.penghaisoft.task.entity.RtnObjNewHollysys;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @Description feign封装调用http接口代码
 * @Author luot
 * @Date 2020/8/17 17:32
 * @Param
 * @return
 **/
@FeignClient(name = "test", url = "${feign.hollysys.baseUrl}", configuration = FeignConfig.class, fallback = FeignClientFallBack.class)
public interface FpjFeignClient {
    @RequestMapping(value = "/ts/v3/datapoint/history/rawvalue", consumes = "application/json;")
    public RtnObjHollysys getHistoryValue(@RequestBody Map<String, Object> map);

    @RequestMapping(value = "/ts/v3/datapoint/current", consumes = "application/json;")
    public RtnObjNewHollysys getcurrentValue(@RequestBody Map<String, Object> map);
}
