package com.penghaisoft.task.feign.client;

import com.penghaisoft.task.entity.RtnObjHollysys;
import com.penghaisoft.task.entity.RtnObjNewHollysys;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @ClassName feign封装调用异常的默认值【此类基本用不到】
 * @Description
 * @Author luo_0
 * @Date 2020/8/17 15:28
 **/
@Component
public class FeignClientFallBack implements FpjFeignClient {
    @Override
    public RtnObjHollysys getHistoryValue(Map<String, Object> map) {
        RtnObjHollysys rtnObj = new RtnObjHollysys();
        rtnObj.setCode("1");
        rtnObj.setMessage("发生异常");
        return rtnObj;
    }

    @Override
    public RtnObjNewHollysys getcurrentValue(Map<String, Object> map) {
        RtnObjNewHollysys rtnObj = new RtnObjNewHollysys();
        rtnObj.setCode("1");
        rtnObj.setMessage("发生异常");
        return rtnObj;
    }
}
