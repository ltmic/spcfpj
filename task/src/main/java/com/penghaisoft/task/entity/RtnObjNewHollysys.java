package com.penghaisoft.task.entity;

import lombok.Data;

import java.util.List;

/**
 * @ClassName RtnObjHollysys
 * @Description
 * @Author luo_0
 * @Date 2020/8/17 18:13
 **/
@Data
public class RtnObjNewHollysys {
    private String code;
    private String message;
    private List<Value> results;
}
