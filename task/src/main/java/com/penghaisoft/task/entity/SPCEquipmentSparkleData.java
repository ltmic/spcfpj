package com.penghaisoft.task.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class SPCEquipmentSparkleData {
    private String dataId;

    private String productno;

    private String equipmentno;

    private BigDecimal amaterialpressureset;//A料注料压力设定值

    private BigDecimal bmaterialpressureset;//B料注料压力设定值

    private BigDecimal vacuumpressureset;//真空度压力设定值

    private BigDecimal gunflowset;//枪头流量设定值

    private BigDecimal abmaterialbalanceset;//A/B料比设定值

    private BigDecimal amaterialtemperatureset;//A料料温设定值

    private BigDecimal bmaterialtemperatureset;//B料料温设定值

    private BigDecimal materialquantityset;//注料量设定值

    private BigDecimal mouldtemperatureset;//模温设定值

    private BigDecimal crockpressureset;

    private BigDecimal abmaterialpressuredifferset;//A/B料压差设定值

    private BigDecimal caseadvancetemperatureset;//壳体预热温度设定值

    private BigDecimal conditiontemperatureset;//环温设定值

    private BigDecimal outconditiontemperset;

    private BigDecimal contractrateset;

    private BigDecimal gumtimeset;

    private BigDecimal freedensityset;

    private BigDecimal pressuredensityset;

    private BigDecimal amaterialpressure;//A料注料压力

    private BigDecimal bmaterialpressure;//B料注料压力

    private BigDecimal vacuumpressure;//真空度压力

    private BigDecimal gunflow;//枪头流量

    private BigDecimal abmaterialbalance;//A/B料比

    private BigDecimal amaterialtemperature;//A料料温

    private BigDecimal bmaterialtemperature;//B料料温

    private BigDecimal materialquantity;//注料量

    private BigDecimal mouldtemperature;//模温

    private BigDecimal crockpressure;

    private BigDecimal abmaterialpressurediffer;//A/B料压差

    private BigDecimal caseadvancetemperature;//壳体预热温度

    private BigDecimal conditiontemperature;//环温

    private BigDecimal outconditiontemper;

    private BigDecimal contractrate;

    private BigDecimal gumtime;

    private BigDecimal freedensity;

    private BigDecimal pressuredensity;

    private Date starttime;

    private Date endtime;

    private Date collecttime;

    private Integer isok;

    private String modelno;
}