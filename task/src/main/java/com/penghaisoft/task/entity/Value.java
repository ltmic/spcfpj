package com.penghaisoft.task.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName Value
 * @Description
 * @Author luo_0
 * @Date 2020/8/19 10:45
 **/
@Data
public class Value {
    private long s;

    private long t;

    private long v;
}
