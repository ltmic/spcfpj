package com.penghaisoft.task.entity;

public class SPCEquipmentSparkleProOrg {
    private String proorgId;

    private String lineno;

    private String stationno;

    private String modelno;

    private String equipmentno;

    private String modelname;

    private String series;

    public String getProorgId() {
        return proorgId;
    }

    public void setProorgId(String proorgId) {
        this.proorgId = proorgId == null ? null : proorgId.trim();
    }

    public String getLineno() {
        return lineno;
    }

    public void setLineno(String lineno) {
        this.lineno = lineno == null ? null : lineno.trim();
    }

    public String getStationno() {
        return stationno;
    }

    public void setStationno(String stationno) {
        this.stationno = stationno == null ? null : stationno.trim();
    }

    public String getModelno() {
        return modelno;
    }

    public void setModelno(String modelno) {
        this.modelno = modelno == null ? null : modelno.trim();
    }

    public String getEquipmentno() {
        return equipmentno;
    }

    public void setEquipmentno(String equipmentno) {
        this.equipmentno = equipmentno == null ? null : equipmentno.trim();
    }

    public String getModelname() {
        return modelname;
    }

    public void setModelname(String modelname) {
        this.modelname = modelname == null ? null : modelname.trim();
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series == null ? null : series.trim();
    }
}