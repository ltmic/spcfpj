package com.penghaisoft.task.entity;

import lombok.Data;

import java.util.List;

/**
 * @ClassName Result
 * @Description
 * @Author luo_0
 * @Date 2020/8/19 10:45
 **/
@Data
public class Result {
    private List<Value> historyData;
    private long continuationPoint;
}
