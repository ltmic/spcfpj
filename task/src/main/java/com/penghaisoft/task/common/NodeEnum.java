package com.penghaisoft.task.common;

/**
 * @ClassName 和利时取数节点信息
 * @Description
 * @Author luo_0
 * @Date 2020/8/18 17:34
 **/
public class NodeEnum {

    /**
     * @Description B1线节点配置信息
     * @ClassName NodeEnum
     * @Author luot
     * @Date 2020/8/18 17:35
     **/
    public enum NodeB1 {
        Tag12("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag12", "注料量设定值", "SymLink-yuzhuangB1", "materialquantityset"),//注料量设定值
        Tag13("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag13", "测量比率设定值", "SymLink-yuzhuangB1", "abmaterialbalanceset"),//A/B料比设定值
        Tag14("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag14", "黑料流量设定值", "SymLink-yuzhuangB1", "gunflowset"),//枪头流量设定值
        //        Tag15("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag15", "白料流量设定值", "SymLink-yuzhuangB1"),
        Tag16("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag16", "黑料罐温设定值", "SymLink-yuzhuangB1", "amaterialtemperatureset"),//A料料温设定值
        Tag17("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag17", "白料罐温设定值", "SymLink-yuzhuangB1", "bmaterialtemperatureset"),//B料料温设定值
        Tag18("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag18", "黑料压力设定值", "SymLink-yuzhuangB1", "amaterialpressureset"),//A料注料压力设定值
        Tag19("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag19", "白料压力设定值", "SymLink-yuzhuangB1", "bmaterialpressureset"),//B料注料压力设定值
        Tag2("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag2", "测量比率实际值", "SymLink-yuzhuangB1", "abmaterialbalance"),//A/B料比
        Tag3("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag3", "黑料压力实际值", "SymLink-yuzhuangB1", "amaterialpressure"),//A料注料压力
        Tag4("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag4", "白料压力实际值", "SymLink-yuzhuangB1", "bmaterialpressure"),//B料注料压力
        Tag5("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag5", "黑料流量实际值", "SymLink-yuzhuangB1", "gunflow"),//枪头流量
        //        Tag6("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag6", "白料流量实际值", "SymLink-yuzhuangB1"),
        Tag20("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag20", "注料量实际值", "SymLink-yuzhuangB1", "materialquantity"),//注料量
        Tag21("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag21", "黑料罐温实际值", "SymLink-yuzhuangB1", "amaterialtemperature"),//A料料温
        Tag22("C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag22", "白料罐温实际值", "SymLink-yuzhuangB1", "bmaterialtemperature");//B料料温

        private final String tag;
        private final String tagDesc;
        private final String nameSpace;
        private final String attr;

        NodeB1(String tag, String tagDesc, String nameSpace, String attr) {
            this.tag = tag;
            this.tagDesc = tagDesc;
            this.nameSpace = nameSpace;
            this.attr = attr;
        }

        public String getTag() {
            return tag;
        }

        public String getNameSpace() {
            return nameSpace;
        }

        public String getTagDesc() {
            return tagDesc;
        }

        public String getAttr() {
            return attr;
        }
    }

    /**
     * @Description B2线节点配置信息
     * @ClassName NodeEnum
     * @Author luot
     * @Date 2020/8/18 17:35
     **/
    public enum NodeB2 {
        Tag12("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag12", "注料量设定值", "SymLink-yuzhuangB1", "materialquantityset"),//注料量设定值
        Tag13("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag13", "测量比率设定值", "SymLink-yuzhuangB1", "abmaterialbalanceset"),//A/B料比设定值
        Tag14("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag14", "黑料流量设定值", "SymLink-yuzhuangB1", "gunflowset"),//枪头流量设定值
        //        Tag15("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag15", "白料流量设定值", "SymLink-yuzhuangB1"),
        Tag16("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag16", "黑料罐温设定值", "SymLink-yuzhuangB1", "amaterialtemperatureset"),//A料料温设定值
        Tag17("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag17", "白料罐温设定值", "SymLink-yuzhuangB1", "bmaterialtemperatureset"),//B料料温设定值
        Tag18("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag18", "黑料压力设定值", "SymLink-yuzhuangB1", "amaterialpressureset"),//A料注料压力设定值
        Tag19("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag19", "白料压力设定值", "SymLink-yuzhuangB1", "bmaterialpressureset"),//B料注料压力设定值
        Tag2("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag2", "测量比率实际值", "SymLink-yuzhuangB1", "abmaterialbalance"),//A/B料比
        Tag3("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag3", "黑料压力实际值", "SymLink-yuzhuangB1", "amaterialpressure"),//A料注料压力
        Tag4("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag4", "白料压力实际值", "SymLink-yuzhuangB1", "bmaterialpressure"),//B料注料压力
        Tag5("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag5", "黑料流量实际值", "SymLink-yuzhuangB1", "gunflow"),//枪头流量
        //        Tag6("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag6", "白料流量实际值", "SymLink-yuzhuangB1"),
        Tag20("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag20", "注料量实际值", "SymLink-yuzhuangB1", "materialquantity"),//注料量
        Tag21("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag21", "黑料罐温实际值", "SymLink-yuzhuangB1", "amaterialtemperature"),//A料料温
        Tag22("C1.FAPAOJI_B2.FAPAOJI_B2_FPJ.Tag22", "白料罐温实际值", "SymLink-yuzhuangB1", "bmaterialtemperature");//B料料温

        private final String tag;
        private final String tagDesc;
        private final String nameSpace;

        private final String attr;

        NodeB2(String tag, String tagDesc, String nameSpace, String attr) {
            this.tag = tag;
            this.tagDesc = tagDesc;
            this.nameSpace = nameSpace;
            this.attr = attr;
        }

        public String getTag() {
            return tag;
        }

        public String getNameSpace() {
            return nameSpace;
        }

        public String getTagDesc() {
            return tagDesc;
        }

        public String getAttr() {
            return attr;
        }
    }
}
