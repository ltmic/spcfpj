package com.penghaisoft.task.controller;

import com.alibaba.fastjson.JSONObject;
import com.penghaisoft.task.entity.RtnObjHollysys;
import com.penghaisoft.task.feign.client.FpjFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName Resttemplate 跟 feign的性能对比， 同一个请求 feign 1000ms级别， Resttemplate20ms级别
 * @Description
 * @Author luo_0
 * @Date 2020/8/17 14:48
 **/
@Component
@Slf4j
public class FeignVSResttemplateController {

    @Autowired
    private FpjFeignClient fpjFeignClient;

    @Value("${feign.hollysys.token}")
    private String token;

    @Autowired
    private RestTemplate restTemplate;

//    @Scheduled(cron = "0/15 * * * * ? ")
    public void scheduledJob() {
        long t1 = System.currentTimeMillis();
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> detailMap = new HashMap<String, Object>();
        detailMap.put("endTime", "1697225413000");
        detailMap.put("maxSizePerNode", 1000);
        detailMap.put("returnBounds", true);
        detailMap.put("startTime", "1597196613000");

        Map<String, Object> nodeMap = new HashMap<String, Object>();
        nodeMap.put("continuationPoint", "");
        nodeMap.put("id", "C1.FAPAOJI_B1.FAPAOJI_B1_FPJ.Tag2");
        nodeMap.put("namespace", "SymLink-yuzhuangB1");

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        list.add(nodeMap);

        map.put("detail", detailMap);
        map.put("nodes", list);

        RtnObjHollysys rtnOb = fpjFeignClient.getHistoryValue(map);
        long t2 = System.currentTimeMillis();
        log.info("feign返回结果: " + rtnOb);
        log.info("feign耗时：" + (t2 - t1));

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("token", token);
            JSONObject param = (JSONObject) JSONObject.toJSON(map);
            HttpEntity<JSONObject> request = new HttpEntity<JSONObject>(param, headers);

            String url = "http://timeseries-haier-haiergroup.cosmoiiot.haier.net/ts/v3/datapoint/history/rawvalue";
            ResponseEntity<JSONObject> wcsResp = restTemplate.postForEntity(url, request, JSONObject.class);
            log.info("restTemplate返回结果：" + wcsResp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        long t3 = System.currentTimeMillis();

        log.info("restTemplate耗时：" + (t3 - t2));
    }
}
