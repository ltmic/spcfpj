package com.penghaisoft.task.controller;

import com.penghaisoft.task.common.NodeEnum;
import com.penghaisoft.task.entity.RtnObjNewHollysys;
import com.penghaisoft.task.entity.SPCEquipmentSparkleData;
import com.penghaisoft.task.entity.Value;
import com.penghaisoft.task.feign.client.FpjFeignClient;
import com.penghaisoft.task.service.ISPCEquipmentSparkleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * @ClassName 调用和利时接口，获取发泡线数据
 * @Description
 * @Author luo_0
 * @Date 2020/8/17 14:48
 **/
@Component
@Slf4j
public class FpjTaskNewController {

    @Autowired
    private FpjFeignClient fpjFeignClient;

    @Autowired
    private ISPCEquipmentSparkleService spcEquipmentSparkleService;

    @Scheduled(cron = "0 0/1 * * * ? ")
    public void scheduledJob() {
        log.info("##########开始处理###########");
//        处理设备编号9010XAXTFP0001，对应 NodeEnum.NodeB1 发泡线
        deal("9010XAXTFP0001");
        log.info("9010XAXTFP0001处理完成");
//        处理设备编号9010XAXTFP0002，对应 NodeEnum.NodeB1 发泡线
        deal("9010XAXTFP0002");
        log.info("9010XAXTFP0002处理完成");
        log.info("##########结束处理###########");
    }

    private void deal(String equipmentno) {
//        开始时间
        long start = 0L;
        SPCEquipmentSparkleData spcEquipmentSparkleData = spcEquipmentSparkleService.selectTop1(equipmentno);
        if (spcEquipmentSparkleData != null) {
            if (spcEquipmentSparkleData.getCollecttime() != null) {
                start = spcEquipmentSparkleData.getCollecttime().getTime();
            }
            log.info(equipmentno + "存在最新一条数据！" + spcEquipmentSparkleData.getCollecttime());
        } else {
            log.info(equipmentno + "不存在数据！");
        }

//        log.info("end" + end);
        Map<String, Object> map = new HashMap<String, Object>();

        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Map<String, Object> nodeMap = new HashMap<String, Object>();

        if ("9010XAXTFP0001".equals(equipmentno)) {
            for (NodeEnum.NodeB1 tmp : NodeEnum.NodeB1.values()) {
                nodeMap = new HashMap<String, Object>();
                nodeMap.put("id", tmp.getTag());
                nodeMap.put("namespace", tmp.getNameSpace());
                list.add(nodeMap);
            }
        } else if ("9010XAXTFP0002".equals(equipmentno)) {
            for (NodeEnum.NodeB2 tmp : NodeEnum.NodeB2.values()) {
                nodeMap = new HashMap<String, Object>();
                nodeMap.put("id", tmp.getTag());
                nodeMap.put("namespace", tmp.getNameSpace());
                list.add(nodeMap);
            }
        }

        map.put("nodes", list);

        RtnObjNewHollysys rtnOb = fpjFeignClient.getcurrentValue(map);

        if (rtnOb != null && "0".equals(rtnOb.getCode())) {
            List<Value> results = rtnOb.getResults();
            if (results != null && !results.isEmpty()) {
                if (results.size() != NodeEnum.NodeB1.values().length) {
                    log.info("和利时返回数据数量不是" + NodeEnum.NodeB1.values().length + "个，而是" + results.size() + "个。");
                    return;
                }

                String compare = "";
                List<SPCEquipmentSparkleData> insertList = new ArrayList<SPCEquipmentSparkleData>();
                Value val0 = results.get(0);

                SPCEquipmentSparkleData tmp = new SPCEquipmentSparkleData();
                tmp.setDataId(UUID.randomUUID().toString());
                tmp.setEquipmentno(equipmentno);
                tmp.setCollecttime(new Date(val0.getT()));

                if ((start - val0.getT()) >= -10 && (start - val0.getT()) <= 10) {
                    log.info("当前所取数据时间戳与数据库一致，无须插入！");
                    return;
                } else {
                    int i = 0;
                    long time = val0.getT();
                    StringBuffer sb = new StringBuffer();
                    for (NodeEnum.NodeB1 b1 : NodeEnum.NodeB1.values()) {
                        Value v = results.get(i);
                        Class cl = tmp.getClass();
                        String fieldName = b1.getAttr();
                        try {
                            Field field = cl.getDeclaredField(fieldName);
                            field.setAccessible(true);
                            field.set(tmp, new BigDecimal(v.getV()));
                        } catch (Exception e) {
//                                    e.printStackTrace();
                        }
                        i++;
                    }
                }

                insertList.add(tmp);
                log.info("待插入" + insertList.size() + "条数据！");

                if (insertList != null && !insertList.isEmpty()) {
                    for (SPCEquipmentSparkleData tmp1 : insertList) {
                        spcEquipmentSparkleService.insertSelective(tmp1);
                    }

                    log.info("插入" + insertList.size() + "条数据！");
                }
            }
        } else {
            log.info("和利时返回失败状态: " + rtnOb);
        }
    }
}
