package com.penghaisoft.task.dao;

import com.penghaisoft.task.entity.SPCEquipmentSparkleData;

public interface SPCEquipmentSparkleDataMapper {
    int insert(SPCEquipmentSparkleData record);

    int insertSelective(SPCEquipmentSparkleData record);

    SPCEquipmentSparkleData selectTop1(String equipmentno);
}