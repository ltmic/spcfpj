package com.penghaisoft.task.dao;

import com.penghaisoft.task.entity.SPCEquipmentSparkleProOrg;

public interface SPCEquipmentSparkleProOrgMapper {
    int deleteByPrimaryKey(String proorgId);

    int insert(SPCEquipmentSparkleProOrg record);

    int insertSelective(SPCEquipmentSparkleProOrg record);

    SPCEquipmentSparkleProOrg selectByPrimaryKey(String proorgId);

    int updateByPrimaryKeySelective(SPCEquipmentSparkleProOrg record);

    int updateByPrimaryKey(SPCEquipmentSparkleProOrg record);
}